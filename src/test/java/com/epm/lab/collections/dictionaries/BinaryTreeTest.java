package com.epm.lab.collections.dictionaries;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;

public class BinaryTreeTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private String keyA = "A";
    private String keyB = "B";
    private String keyC = "C";
    private String valueA = "AAA";
    private String valueB = "BBB";
    private String valueC = "CCC";
    BinaryTree<String, String> tree1 = new BinaryTree<>();
    BinaryTree<String, String> tree2;

    @Before
    public void init(){
        tree1 = new BinaryTree<>();
    }

    @Test
    public void testConstructorWithMap() {
        //GIVEN
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        int expected = 2;
        //WHEN
        tree2 = new BinaryTree<>(tree1);
        int actual = tree2.size();
        //THEN
        Assert.assertNotNull(tree2);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testSize() {
        //GIVEN
        int expected = 0;
        //WHEN
        int actual = tree1.size();
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        //WHEN
        tree1.put(null, valueA);
    }

    @Test
    public void testPutWithNullValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        //WHEN
        tree1.put(keyA, null);
    }

    @Test
    public void testPutWithNullKeyAndValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        //WHEN
        tree1.put(null, null);
    }

    @Test
    public void testPutAndSize() {
        //GIVEN
        int expected = 2;
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        //WHEN
        int actual = tree1.size();
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutAndSize1() {
        //GIVEN
        int expected = 2;
        tree1.put(keyA, "A_A_A");
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        //WHEN
        int actual = tree1.size();
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testGetWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        tree1.put(keyA, valueA);
        //WHEN
        tree1.get(null);
    }

    @Test
    public void testGetFromEmptyBinaryTree() {
        //GIVEN
        //WHEN
        String actual = tree1.get(keyA);
        //THEN
        Assert.assertNull(actual);
    }

    @Test
    public void testGet() {
        //GIVEN
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        tree1.put(keyC, valueC);
        //WHEN
        String stringA = tree1.get(keyA);
        String stringB = tree1.get(keyB);
        String stringC = tree1.get(keyC);
        //THEN
        Assert.assertNotNull(stringA);
        Assert.assertNotNull(stringB);
        Assert.assertNotNull(stringC);
        Assert.assertThat(stringA, is(valueA));
        Assert.assertThat(stringB, is(valueB));
        Assert.assertThat(stringC, is(valueC));
    }

    @Test
    public void testEqualsItself() {
        //GIVEN
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        tree1.put(keyC, valueC);
        //WHEN
        Boolean actual = tree1.equals(tree1);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertTrue(actual);
    }

    @Test
    public void testEqualsWithNull() {
        //GIVEN
        tree2 = null;
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        tree1.put(keyC, valueC);
        //WHEN
        Boolean actual = tree1.equals(tree2);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertNull(tree2);
        Assert.assertFalse(actual);
    }

    @Test
    public void testEqualsTrue() {
        //GIVEN
        tree2 = new BinaryTree<>();
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        tree1.put(keyC, valueC);
        tree2.put(keyA, valueA);
        tree2.put(keyB, valueB);
        tree2.put(keyC, valueC);
        //WHEN
        Boolean actual12 = tree1.equals(tree2);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertNotNull(tree2);
        Assert.assertTrue(actual12);
    }

    @Test
    public void testEqualsFalse() {
        //GIVEN
        tree2 = new BinaryTree<>();
        //WHEN
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        tree1.put(keyC, valueC);
        tree2.put(keyA, valueA);
        tree2.put(keyB, valueB);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertNotNull(tree2);
        Assert.assertTrue(tree1.equals(tree1));
        Assert.assertTrue(tree2.equals(tree2));
        Assert.assertFalse(tree1.equals(tree2));
        Assert.assertFalse(tree2.equals(tree1));
    }

    @Test
    public void testHashCode() {
        //GIVEN
        tree2 = new BinaryTree<>();
        //WHEN
        tree1.put(keyA, valueA);
        tree1.put(keyB, valueB);
        tree1.put(keyC, valueC);
        tree2.put(keyA, valueA);
        tree2.put(keyB, valueB);
        tree2.put(keyC, valueC);
        //THEN
        Assert.assertEquals(tree1.hashCode(), tree2.hashCode());
    }
}