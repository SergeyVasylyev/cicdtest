package com.epm.lab.collections.dictionaries;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;

public class HashTableTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private String keyA = "A";
    private String keyB = "B";
    private String keyC = "C";
    private String valueA = "AAA";
    private String valueB = "BBB";
    private String valueC = "CCC";
    HashTable<String, String> table1;
    HashTable<String, String> table2;

    @Before
    public void init(){
        table1 = new HashTable<>();
    }

    @Test
    public void testConstructorWithMap() {
        //GIVEN
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        int expected = 2;
        table2 = new HashTable<>(table1);
        //WHEN
        int actual = table2.size();
        //THEN
        Assert.assertNotNull(table2);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testSize() {
        //GIVEN
        int expected = 0;
        //WHEN
        int actual = table1.size();
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        //WHEN
        table1.put(null, valueA);
    }

    @Test
    public void testPutWithNullValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        //WHEN
        table1.put("A", null);
    }

    @Test
    public void testPutWithNullKeyAndValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        //WHEN
        table1.put(null, null);
    }

    @Test
    public void testPutAndSize() {
        //GIVEN
        int expected = 2;
        //WHEN
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        int actual = table1.size();
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutAndSize1() {
        //GIVEN
        int expected = 2;
        table1.put(keyA, "A_A_A");
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        //WHEN
        int actual = table1.size();
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testGetWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        table1.put(keyA, valueA);
        //WHEN
        table1.get(null);
    }

    @Test
    public void testGetFromEmptyHashTable() {
        //GIVEN
        //WHEN
        String actual = table1.get(keyA);
        //THEN
        Assert.assertNull(actual);
    }

    @Test
    public void testGet() {
        //GIVEN
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        table1.put(keyC, valueC);
        //WHEN
        String stringA = table1.get(keyA);
        String stringB = table1.get(keyB);
        String stringC = table1.get(keyC);
        //THEN
        Assert.assertNotNull(stringA);
        Assert.assertNotNull(stringB);
        Assert.assertNotNull(stringC);
        Assert.assertThat(stringA, is(valueA));
        Assert.assertThat(stringB, is(valueB));
        Assert.assertThat(stringC, is(valueC));
    }

    @Test
    public void testEqualsItself() {
        //GIVEN
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        table1.put(keyC, valueC);
        //WHEN
        Boolean actual = table1.equals(table1);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertTrue(actual);
    }

    @Test
    public void testEqualsWithNull() {
        //GIVEN
        table2 = null;
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        table1.put(keyC, valueC);
        //WHEN
        Boolean actual = table1.equals(table2);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertNull(table2);
        Assert.assertFalse(actual);
    }

    @Test
    public void testEqualsTrue() {
        //GIVEN
        table2 = new HashTable<>();
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        table1.put(keyC, valueC);
        table2.put(keyA, valueA);
        table2.put(keyB, valueB);
        table2.put(keyC, valueC);
        //WHEN
        Boolean actual = table1.equals(table2);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertNotNull(table2);
        Assert.assertTrue(actual);
    }

    @Test
    public void testEqualsFalse() {
        //GIVEN
        table2 = new HashTable<>();
        //WHEN
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        table1.put(keyC, valueC);
        table2.put(keyA, valueA);
        table2.put(keyB, valueB);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertNotNull(table2);
        Assert.assertTrue(table1.equals(table1));
        Assert.assertTrue(table2.equals(table2));
        Assert.assertFalse(table1.equals(table2));
        Assert.assertFalse(table2.equals(table1));
    }

    @Test
    public void testHashCode() {
        //GIVEN
        table2 = new HashTable<>();
        //WHEN
        table1.put(keyA, valueA);
        table1.put(keyB, valueB);
        table1.put(keyC, valueC);
        table2.put(keyA, valueA);
        table2.put(keyB, valueB);
        table2.put(keyC, valueC);
        //THEN
        Assert.assertEquals(table1.hashCode(), table2.hashCode());
    }
}