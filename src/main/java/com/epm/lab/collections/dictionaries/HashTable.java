package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;

import java.util.Iterator;

public class HashTable<K, V> implements Map<K, V> {

    private LList<Entry<K, V>>[] BucketArray;
    private int size = 0;
    private static final int DEFAULTSIZE = 11;

    public HashTable() {
        BucketArray = new LList[DEFAULTSIZE];
    }

    public HashTable(Map<K, V> map) {
        BucketArray = new LList[map.size() * 2];

        for (Entry<K, V> entry : map) {
            put(entry.key, entry.value);
        }
    }

    @Override
    public V get(K key) {
        if (key == null)
            throw new NullPointerException();
        int index = (key.hashCode() & Integer.MAX_VALUE) % BucketArray.length;
        if (BucketArray[index] != null) {
            for (Entry<K, V> entry : BucketArray[index]) {
                if (entry.key.equals(key)) {
                    return entry.value;
                }
            }
        }
        return null;
    }

    @Override
    public void put(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException();
        }

        int index = (key.hashCode() & Integer.MAX_VALUE) % BucketArray.length;
        if (BucketArray[index] != null) {
            for (Entry<K, V> entry : BucketArray[index]) {
                if (entry.key.equals(key)) {
                    entry.value = value;
                    return;
                }
            }
        } else {
            BucketArray[index] = new LList<>();
        }
        BucketArray[index].add(new Entry(key, value));
        size++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof HashTable)) {
            return false;
        }

        HashTable<?, ?> hashTable = (HashTable<?, ?>) o;
        if (BucketArray.length != hashTable.BucketArray.length) {
            return false;
        }
        if (size() != hashTable.size) {
            return false;
        }
        for (int i = 0; i < BucketArray.length; i++) {
            if ( BucketArray[i] != null
              && hashTable.BucketArray[i] != null
              && !(BucketArray[i].equals(hashTable.BucketArray[i])))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Entry<K, V> entry : entrySet()) {
            hashCode += (entry.key.hashCode() & Integer.MAX_VALUE);
        }
        return hashCode;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return entrySet().iterator();
    }

    private LList<Entry<K, V>> entrySet() {
        LList<Entry<K, V>> entryList = new LList<>();
        for (int i = 0; i < BucketArray.length; i++) {
            if (BucketArray[i] != null) {
                for (Entry<K, V> entry : BucketArray[i]) {
                    entryList.add(new Entry<>(entry.key, entry.value));
                }
            }
        }
        return entryList;
    }
}
