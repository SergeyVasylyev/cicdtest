package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.validators.impl.ValidationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class LList<E> implements Iterable<E> {

    private static final Logger logger = LoggerFactory.getLogger(LList.class);

    private Link firstLink;
    private Link lastLink;
    private int size;

    class Link<E> {
        public Link next;
        private E linkValue;

        public Link(E e) {
            linkValue = e;
        }

        public void display() {
            logger.info("Object: " + linkValue.toString());
        }

        @Override
        public String toString() {
            return linkValue.toString();
        }

        public E getValue() {
            return linkValue;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return (Iterator<E>) new ListIter(firstLink);
    }

    class ListIter implements Iterator<E> {
        Link p = null;

        public ListIter(Link p) {
            this.p = p;
        }

        @Override
        public boolean hasNext() {
            return (p != null);
        }

        @Override
        public E next() {
            E result = (E) p.getValue();
            p = p.next;
            return result;
        }
    }

    public LList() {
        firstLink = null;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return (firstLink == null);
    }

    public boolean add(E element) throws NullPointerException{
        ValidationImpl validation = new ValidationImpl();
        validation.validateValue(element);

        Link newLink = new Link(element);
        if (isEmpty()) {
            firstLink = newLink;
            lastLink = newLink;
        } else {
            lastLink.next = newLink;
            lastLink = newLink;
        }
        size++;
        return true;
    }

    public boolean addFirst(E element) throws NullPointerException {
        ValidationImpl validation = new ValidationImpl();
        validation.validateValue(element);

        Link theLink = firstLink;
        Link newLink = new Link(element);
        firstLink = newLink;
        newLink.next = theLink;
        size++;
        return true;
    }

    public void add(int index, E element) throws IllegalArgumentException, NullPointerException {
        ValidationImpl validation = new ValidationImpl();
        validation.validateIndex(index, size - 1);
        validation.validateValue(element);

        int curIndex = 0;
        Link theLink = firstLink;
        Link newLink = new Link(element);
        Link exLink = theLink;
        if (index == 0) {
            addFirst(element);
            return;
        }
        while (theLink != null) {
            if (curIndex == index) {
                exLink.next = newLink;
                newLink.next = theLink;
                size++;
                break;
            }
            exLink = theLink;
            theLink = theLink.next;
            curIndex++;
        }
        if (index == size) {
            lastLink.next = newLink;
            lastLink = newLink;
            size++;
        }
    }

    public E set(int index, E element) throws IllegalArgumentException, NullPointerException {
        ValidationImpl validation = new ValidationImpl();
        validation.validateIndex(index, size - 1);
        validation.validateValue(element);

        int curIndex = 0;
        Link theLink = firstLink;
        Link newLink = new Link(element);
        Link exLink = theLink;
        if (index == 0) {
            firstLink = newLink;
            newLink.next = theLink.next;
            return (E) newLink.getValue();
        }
        while (theLink != null) {
            if (curIndex == index) {
                if (curIndex == size - 1) {
                    exLink.next = newLink;
                    lastLink = newLink;
                    break;
                }
                exLink.next = newLink;
                newLink.next = theLink.next;
                break;
            }
            exLink = theLink;
            theLink = theLink.next;
            curIndex++;
        }
        return (E) newLink.getValue();
    }

    public E get(int index) throws IllegalArgumentException{
        ValidationImpl validation = new ValidationImpl();
        validation.validateIndex(index, size - 1);

        int curIndex = 0;
        Link theLink = firstLink;
        if (index == 0) {
            return getFirst();
        } else if (index == size - 1) {
            return getLast();
        }
        while (theLink != null) {
            if (curIndex == index) {
                return (E) theLink.getValue();
            }
            theLink = theLink.next;
            curIndex++;
        }
        return null;
    }

    public E getFirst() {
        if (isEmpty()) {
            logger.error("The List is Empty!");
            return null;
        } else {
            return (E) firstLink.getValue();
        }
    }

    public E getLast() {
        if (isEmpty()) {
            logger.error("The List is Empty!");
            return null;
        } else {
            return (E) lastLink.getValue();
        }
    }

    public int indexOf(E element) throws NullPointerException{
        ValidationImpl validation = new ValidationImpl();
        validation.validateValue(element);

        int curIndex = -1;
        boolean indexFound = false;
        Link theLink = firstLink;
        while (theLink != null) {
            if (theLink.getValue().equals(element)) {
                indexFound = true;
                curIndex++;
                break;
            }
            theLink = theLink.next;
            curIndex++;
        }
        return indexFound ? curIndex : -1;
    }

    public E remove(int index) throws IndexOutOfBoundsException{
        ValidationImpl validation = new ValidationImpl();
        validation.validateIndex(index, size - 1);

        int curIndex = 0;
        Link theLink = firstLink;
        Link exLink = theLink;
        if (index == 0) {
            firstLink = theLink.next;
            size--;
            return (E) theLink.getValue();
        }
        while (theLink != null) {
            if (curIndex == index) {
                if (curIndex == size - 1) {
                    exLink.next = null;
                    lastLink = exLink;
                    size--;
                    break;
                }
                exLink.next = theLink.next;
                theLink.next = null;
                size--;
                break;
            }
            exLink = theLink;
            theLink = theLink.next;
            curIndex++;
        }
        return (E) theLink.getValue();
    }

    public void display() {
        Link theLink = firstLink;
        while (theLink != null) {
            theLink.display();
            logger.info("Next Link: " + theLink.next);
            theLink = theLink.next;
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof LList)) {
            return false;
        }
        LList<?> llist = (LList<?>) o;
        if (size != llist.size){
            return false;
        }
        for (int i = 0; i < size; i++) {
            if(!(get(i).equals(llist.get(i)))) {
                return false;
            }
        }
        return true;
    }
}

