package com.epm.lab.collections.dictionaries;

import com.epm.lab.collections.Map;

public class Demo {

    public static void main(String[] args) {

        mapDemo(new HashTable<>());
        mapDemo(new BinaryTree<>());

    }

    public static void mapDemo(Map<String, String> map){
        map.put("contact1", "000-00-00");
        map.put("contact2", "111-11-11");
        map.put("contact2", "222-22-22");
        map.put("contact3", "333-33-33");

        String[] keys = new String[map.size()];

        int i = 0;
        for (Map.Entry<String, String> mapEntry : map) {
            keys[i++] = mapEntry.key;
        }

        for (String key : keys) {
            System.out.println("Key: " + key + ", value: " + map.get(key));
        }
    }
}
