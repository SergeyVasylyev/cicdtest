package com.epm.lab.collections.validators;

public interface Validation<E> {

    void validateIndex(int index, int sizeToCheck) throws IndexOutOfBoundsException;

    void validateValue(E element) throws NullPointerException;

    void checkALength(int size, int alength) throws IndexOutOfBoundsException;

}
