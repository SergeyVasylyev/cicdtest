package com.epm.lab.collections.validators.impl;

import com.epm.lab.collections.validators.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationImpl<E> implements Validation<E> {

    private static final Logger logger = LoggerFactory.getLogger(Validation.class);
    private static final String BEMessage = "Array length is maximum. No more elements will be added!";

    public void validateIndex(int index, int sizeToCheck) throws IndexOutOfBoundsException {
        if (index < 0) {
            String msg = "Wrong index of array. Expected value > 0. Inputed value: " + index;
            logger.error(msg);
            throw new IndexOutOfBoundsException(msg);
        } else if (index > sizeToCheck + 1) {
            String msg = "Wrong index of array. Index > array size (" + (sizeToCheck + 1) + "). Inputed value: " + index;
            logger.error(msg);
            throw new IndexOutOfBoundsException(msg);
        }
    }

    public void validateValue(E element) throws NullPointerException {
        if (element == null) {
            String msg = "Wrong element value. Null values are forbidden!";
            logger.error(msg);
            //throw new BusinessException(msg);
            throw new NullPointerException(msg);
        }
    }

    public void checkALength(int size, int alength) throws IndexOutOfBoundsException {
        if (size + 1 == alength) {
            logger.error(getBEMessage());
            throw new IndexOutOfBoundsException(getBEMessage());
        }
    }

    public static String getBEMessage() {
        return BEMessage;
    }
}
